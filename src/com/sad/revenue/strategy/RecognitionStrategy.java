package com.sad.revenue.strategy;

import com.sad.revenue.Contract;

public abstract class RecognitionStrategy {
	public abstract void calculateRevenueRecognitions(Contract contract);
}
