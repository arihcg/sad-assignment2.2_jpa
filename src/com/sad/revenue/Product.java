package com.sad.revenue;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Product
 *
 */
@Entity
public class Product implements Serializable {

	@Id
	@GeneratedValue
	private long id;
	private String name;
	private String type;
	private static final long serialVersionUID = 1L;

	public Product() {
		super();
	}

	public Product(String name, String type) {
		super();
		this.name = name;
		this.type = type;

	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getId() {
		return id;
	}
}
