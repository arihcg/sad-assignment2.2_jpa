package com.sad.revenue;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

@Entity
public class RevenueRecognition implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private long id;
	@Embedded
	private Money amount;
	@Embedded
	private MfDate date;
	@ManyToOne
	private Contract contract;

	public RevenueRecognition() {
	}

	public RevenueRecognition(Money amount, MfDate date) {
		this.amount = amount;
		this.date = date;
	}
	
	public RevenueRecognition(Contract contract, Money amount, MfDate date) {
		this.contract = contract;
		this.amount = amount;
		this.date = date;
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public MfDate getDate() {
		return date;
	}

	public void setDate(MfDate date) {
		this.date = date;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}
