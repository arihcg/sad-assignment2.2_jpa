package com.sad.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sad.handler.PersistenceHandler;
import com.sad.revenue.Contract;
import com.sad.revenue.Product;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

public class HandlerTests {

	@Test
	public void testProduct() {
		PersistenceHandler pc = new PersistenceHandler();
		
		Product product = new Product("new Product", "W");
		pc.insertProduct(product);
		
		Product other = pc.findProduct(product.getId());
		assertEquals(product.getName(), other.getName());
		pc.close();
	}
	@Test
	public void testContract() {
		PersistenceHandler pc = new PersistenceHandler();
		
		Product product = new Product("new Product", "W");
		Contract contract = new Contract();
		contract.setRevenue(new Money(99.99));
		contract.setProduct(product);
		
		pc.insertContract(contract);
		
		Contract other = pc.findContract(contract.getId());
		assertNotNull(other.getId());
		assertEquals(contract.getProduct(), other.getProduct());
		pc.close();
	}
	
	@Test
	public void testRevenueRecognition() {
		PersistenceHandler pc = new PersistenceHandler();
		
		RevenueRecognition rev = new RevenueRecognition();
		rev.setAmount(new Money(99.99));
		
		pc.insertRecognition(rev);
		
		
		RevenueRecognition other = pc.findRevenueRecognition(rev.getId());
		assertNotNull(other.getId());
		assertEquals(rev.getId(), other.getId());
		pc.close();
	}
	
	

}
