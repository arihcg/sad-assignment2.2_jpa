package com.sad.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.Test;

import com.sad.revenue.Contract;
import com.sad.revenue.Product;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.Money;

public class PersistenceTests {

	@Test
	public void test() {
		assertEquals(2, 2);
	}

	@Test
	public void testProduct() {

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("JPAExample");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Product product = new Product("new Product", "W");
		em.persist(product);
		tx.commit();
		em.close();
		assertTrue(product.getId() > 0);
	}

	@Test
	public void testProductAgain() {

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("JPAExample");
		EntityManager entityManager = emf.createEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();

		Product product = new Product();
		product.setName("TestProduct 101");
		product.setType("W");
		entityManager.persist(product);
		tx.commit();
		assertNotNull(product.getId());

		Product other = (Product) entityManager.find(Product.class,
				product.getId());
		assertNotNull(other.getId());
		assertEquals(true, product.equals(other));
		entityManager.close();
	}
	
	@Test
	public void testContract() {

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("JPAExample");
		EntityManager entityManager = emf.createEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();

		Contract contract = new Contract();
		contract.setRevenue(new Money(99.99));
		entityManager.persist(contract);
		tx.commit();
		
		assertNotNull(contract.getId());
		assertNotNull(contract.getRevenue());

		Contract other = (Contract) entityManager.find(Contract.class,
				contract.getId());
		
		assertNotNull(other.getId());
		assertEquals(contract.getProduct(), other.getProduct());
		entityManager.close();
	}
	
	@Test
	public void testRevenueRecognition() {

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("JPAExample");
		EntityManager entityManager = emf.createEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();

		RevenueRecognition rev = new RevenueRecognition();
		rev.setAmount(new Money(99.99));
		entityManager.persist(rev);
		tx.commit();
		
		assertNotNull(rev.getId());
		assertNotNull(rev.getAmount());

		RevenueRecognition other = (RevenueRecognition) entityManager.find(
				RevenueRecognition.class, rev.getId());
		assertNotNull(other.getId());
		entityManager.close();
	}

}
