package com.sad.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.Test;

import com.sad.handler.PersistenceHandler;
import com.sad.handler.RevenueRecognitionHandler;
import com.sad.revenue.Contract;
import com.sad.revenue.Product;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

public class RevenueHandlerTests {

	@Test
	public void testCalculateRevenueRecognitions() {
		PersistenceHandler ph = new PersistenceHandler();
		RevenueRecognitionHandler handler = new RevenueRecognitionHandler();
		Product word = new Product("TEST: Word Processor 2014", "W");
		Money revenue = new Money(2500.50);
		MfDate dateSigned = new MfDate(2014, 2, 8);
		Contract contract = new Contract(revenue, dateSigned, word);
		long contractID = ph.insertContract(contract);
		assertNotNull("Contract is null", contractID);
		Money result = null;
		handler.calculateRevenueRecognitions(contract);
		result = handler.recognizedRevenue(contract, dateSigned);
		// System.out.println(result.amount().toString());
		assertEquals(new BigDecimal(new BigInteger("250050"), 2),
				result.amount());
	}
}
