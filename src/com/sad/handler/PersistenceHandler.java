package com.sad.handler;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.sad.revenue.Contract;
import com.sad.revenue.Product;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

public class PersistenceHandler {

	private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tx;

	public PersistenceHandler() {
		emf = Persistence.createEntityManagerFactory("JPAExample");
		em = emf.createEntityManager();
		tx = em.getTransaction();
	}

	public long insertProduct(Product product) {
		tx.begin();
		em.persist(product);
		tx.commit();
		return product.getId();
	}

	public Product findProduct(long id) {
		Product product = em.find(Product.class, id);
		return product;
	}

	public long insertContract(Contract contract) {
		tx.begin();
		em.persist(contract);
		tx.commit();
		return contract.getId();
	}

	public Contract findContract(long id) {
		return em.find(Contract.class, id);
	}
	
	public long insertRecognition(Contract contract, Money money, MfDate on )
	{
		RevenueRecognition rev = new RevenueRecognition(contract, money, on);
//		RevenueRecognition rev = new RevenueRecognition();
//		rev.setContract(contract);
//		rev.setAmount(money);
//		rev.setDate(on);
		contract.addRevenueRecognition(rev);
//		tx.begin();
//		em.persist(contract);
//		tx.commit();
		return insertRecognition(rev);
	}

	public long insertRecognition(RevenueRecognition rev) {
		tx.begin();
		em.persist(rev);
		tx.commit();
		return rev.getId();
	}

	public RevenueRecognition findRevenueRecognition(long id) {
		return em.find(RevenueRecognition.class, id);
	}

	public Collection<RevenueRecognition> findRevenueRecognitionsFor(Contract contract) {
		return contract.getRevenueRecognitions();
	}
	public void close(){
		em.close();
	}

}
