package com.sad.handler;

import java.util.Collection;
import java.util.Iterator;

import com.sad.revenue.Contract;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

public class RevenueRecognitionHandler {
	
	private PersistenceHandler ph = new PersistenceHandler();
	
	public Money recognizedRevenue(Contract contract, MfDate asOf) {
		Money result = Money.dollars(0);

		Collection<RevenueRecognition> revenueRecognitions = ph
				.findRevenueRecognitionsFor(contract);
//		System.out.println("SIZE: " + revenueRecognitions.size());
		Iterator<RevenueRecognition> it = revenueRecognitions.iterator();
		while (it.hasNext()) {
			RevenueRecognition rev = it.next();
			result = result.add(rev.getAmount());
		}
		return result;
	}
	
	public void calculateRevenueRecognitions(Contract contract) {
		// Contract contract = rs.findContract(contractNumber);
		Money totalRevenue = contract.getRevenue();
		MfDate recognitionDate = contract.getWhenSigned();
		String type = contract.getProduct().getType();

		if (type.equals("S")) {
			Money[] allocation = totalRevenue.allocate(3);
			ph.insertRecognition(contract, allocation[0], recognitionDate);
			ph.insertRecognition(contract, allocation[1],
					recognitionDate.addDays(60));
			ph.insertRecognition(contract, allocation[2],
					recognitionDate.addDays(90));
		} else if (type.equals("W")) {
			ph.insertRecognition(contract, totalRevenue, recognitionDate);
		} else if (type.equals("D")) {
			Money[] allocation = totalRevenue.allocate(3);
			ph.insertRecognition(contract, allocation[0], recognitionDate);
			ph.insertRecognition(contract, allocation[1],
					recognitionDate.addDays(60));
			ph.insertRecognition(contract, allocation[2],
					recognitionDate.addDays(90));
		}

	}
}
